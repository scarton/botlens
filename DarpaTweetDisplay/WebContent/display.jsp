<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="fetch_user_data.jsp" %>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="tweetdisplay.css"> 
<link rel="stylesheet" href="linegraph.css"> 

<title>TwitterLens display</title>
<%
String userID = request.getParameter("user_id");
String userJSP = fetch_data(userID).toString();%>
<script>
	var user_id = '<%=userID%>';
	var testUser = JSON.parse('<%=userJSP%>');

</script>
</head>
<body>


<div id = "content">
	<div id="profile_history" class="top_three">
		<table class="content_table"><tr>
			<td class = "profile_td"><div id="profile" class="content_div">
				<div class="section_title">Profile</div>
				<div class="inner_div">
				
				</div>
			</div></td>
			<td><div id="profile_timeline" class="content_div">
				<div class="section_title">Profile timeline</div>
				<div class="inner_div">
				
				</div>
			</div></td>
		</tr></table>
	</div>
	<div class = "top_three">
	<table class="content_table"><tr><td>
		<div id="tweet_history" class="content_div"> 
			<div class="section_title">Tweet count</div>
			<div class="graph_mode">
			<table>
			<tr>
				<td><input type="radio" name="th" onclick="showCumulativeTweetHistory()" checked>cumulative</td>
				<td><input type="radio" name="th" onclick="showDayByDayTweetHistory()">by six-hour interval</td>
			</tr>
			</table>
			</div>
			<div class="inner_div">
			
			</div>
		</div>
	</td></tr></table>
	</div>
	<div class = "top_three">
	<table class="content_table"><tr><td>
		<div id="follower_history" class="content_div">
			<div class="section_title">Follower count</div>
			<div class="graph_mode">
			<table>
			<tr>
				<td><input type="radio" name="frh" onclick="showCumulativeFollowerHistory()" checked>cumulative</td>
				<td><input type="radio" name="frh" onclick="showDayByDayFollowerHistory()">by six-hour interval</td>
			</tr>
			</table>
			</div>
			<div class="inner_div">
			
			</div>
		</div>
	</td></tr></table>
	</div>
	<div class = "top_three">
	<table class="content_table"><tr><td>
	
		<div id="followed_history" class="content_div">
			<div class="section_title">Followed count</div>
			<div class="graph_mode">
			<table>
			<tr>
				<td><input type="radio" name="fdh" onclick="showCumulativeFollowedHistory()" checked>cumulative</td>
				<td><input type="radio" name="fdh" onclick="showDayByDayFollowedHistory()">by six-hour interval</td>
			</tr>
			</table>
			</div>
			<div class="inner_div">
			
			</div>
		</div> 
	</td></tr></table>
	</div>
	<div id="lists" class="top_three">
	<table class="content_table"><tr><td>
	
		<div id="item_lists" class="content_div">
			<div class="section_title">Tweets</div>

			<div class="inner_div">
				
			</div>
		</div>
	</td></tr></table>
	</div>
</div>
<div id="header" class="top">
	<img src="SamCheng_banner_partial_12.png"></img>
</div>
<script src="d3_49.min.js"></script>
<script type="text/javascript" src="jquery-latest.js"></script> 
<script type="text/javascript" src="jquery.tablesorter.js"></script> 
<script type="text/javascript" src="d3.legend.js"></script> 
<script src="displayuserinfo.js"></script>
</body>
</html>