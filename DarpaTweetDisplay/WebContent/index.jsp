<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="tweetdisplay.css"> 
<title>TwitterLens search</title>
</head>
<body>

<div id="content">
	<div id="outer_search_div">
		<div id="inner_search_div">
			<div class="section_title">Twitter user ID search</div>
			<br><br>
			<div id="inner_inner_search_div">
				<form action="display.jsp">
				  <input type="submit" value="Search" id="search_button">
				  <input type="text" name="user_id" size="50" id="search_bar">
				</form>
			</div>
			<br><br>
		</div>
	</div>
</div>
<div id="header" class="top">
	<img src="SamCheng_banner_partial_12.png"></img>
</div>
</body>
</html>