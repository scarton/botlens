/**
 * This script generates a dynamic display of information about a Twitter user
 */

var twitterDateParser = d3.time.format("%Y-%m-%d %H:%M:%S.0").parse;
var shortDateParser = d3.time.format("%Y-%m-%d %H:%M:%S.0").parse;

console.log("Trying to parse "+parseShortDate("2015-02-17 01:10:00.0"));

var dateAndTime = d3.time.format("%Y-%m-%d<br>%I:%M %p");
var dateAndTimeNoCR = d3.time.format("%Y-%m-%d %I:%M %p");
var dateOnly = d3.time.format("%Y-%m-%d");
var interval = 1000*3600;

var margin = {top: 30, right: 20, bottom: 30, left: 50};
var width = 900 - margin.left - margin.right;
var height = 300 - margin.top - margin.bottom;

//Define what ids of the HTML are named what
var css_id={
	profile: "profile",
	profile_timeline: "profile_timeline",
	tweet_history: "tweet_history",
	follower_history: "follower_history",
	followed_history: "followed_history",
	item_lists:"item_lists",
	tweet_list:"tweet_list",
	follower_list:"follower_list",
	followed_list:"followed_list",
	tweet_list_button:"tweet_list_button",
	edge_list_button:"edge_list_button",
	list_choice:"list_choice",
	cumulative_tweet_history:"cumulative_tweet_history",
	daybyday_tweet_history: "daybyday_tweet_history",
	cumulative_follower_history:"cumulative_follower_history",
	daybyday_follower_history: "daybyday_follower_history",
	cumulative_followed_history:"cumulative_followed_history",
	daybyday_followed_history: "daybyday_followed_history",
	

};

var css_class = {
	inner:"inner_div",
	profile_image:"profile_image",
	info_table:"info_table",
	info_td:"info_td",
	item_list:"item_list",
	list_button:"list_button",
	selected:"selected",
	unselected:"unselected",
	hidden:"hidden",
	count_history:"count_history",
	title:"title"
};


//Display fields for basic profile info
var profile_display_fields = {	
	'ID':'id', 
	'Screen name':'screen_name', 
	'Location':'location', 
	'Name':'name', 
	'Description':'description', 
	'Home page':'url', 
	'Protected?':'protected', 
	'Followers':'followers_count', 
	'Followed by':'friends_count', 
	'Tweets':'statuses_count', 
	'Creation date':'first_seen',
	'Last updated':'last_seen',
};

//How to display tweets in tweet list
var tweet_list_columns = {
	' ':function(d){return {content:"",value:"Sum: "};},
	'Text':function(d){return {content:d.text,value:1};},
	'Time':function(d){return {content:dateAndTimeNoCR(parseTwitterDate(d.created_at)),value:"N/A"};},
	'Location':function(d){return {content:geoLookup(d.latitude,d.longitude),value:"N/A"};},
	'Reply?':function(d){if (d.in_reply_to_screen_name)
							return {content:d.in_reply_to_screen_name,value:1};
						else
							return {content:"no",value:0};},
	'Retweet?':function(d){if (d.retweet_status_id)
								return {content:d.retweet_status_id,value:1};
							else
								return {content:"no",value:0};},
	'Retweeted':function(d){return {content:d.retweet_count,value:d.retweet_count};},
	'Favorited':function(d){return {content:d.favorites_count,value:d.favorites_count};},
};

var follower_list_columns = {
	'Follower ID':function(d){return {content:d.follower_id,value:1};},
	'Date Added':function(d){return {content:d.time,value:"N/A"};},
};

var followed_list_columns = {
	'Followed ID':function(d){return {content:d.followed_id,value:1};},
	'Date Added':function(d){return {content:d.time,value:"N/A"};},
};

//The user_id variable is defined in the HTML from the URL. The user variable is
//loaded from a web API provided by Cheng. It's a big multi-part JSON object
//representing the history of a Twitter user. See test_profile.json for an example

var user = null;
var AJAX = createXMLHttpRequest();
getDataAndInitializeApplication(user_id);



/**
 * Function definitions
 */



//Get the data and, when you get it, initialize the application
function getDataAndInitializeApplication(userID)
{
	var start = new Date().getTime();
	AJAX.onreadystatechange = initializeApplication;
	AJAX.open("GET", "http://riemann.si.umich.edu:8080/TwitterBots/UserInfo?requestType=userInfo&userId=" + userID);
	AJAX.send("");
}

//Initialize the application based on the retrieved data
function initializeApplication()
{
	if (AJAX.readyState == 4 && AJAX.status == 200)
	{
		console.log("Loading data from server...");
		user = eval('(' + AJAX.responseText + ')');
		//user = testUser;
		console.log(user);
		if (!jQuery.isEmptyObject(user) && !jQuery.isEmptyObject(user.count_history))
		{
			createProfileDisplay();
			createProfileTimeLineDisplay();
			
			createTweetHistoryDisplay();
			createFollowerHistoryDisplay();
			createFollowedHistoryDisplay();
			
			createTweetListDisplay();
/*			createFollowerListDisplay();
			createFollowedListDisplay();*/
			showList(css_id.tweet_list_button, css_id.tweet_list);

		}
		else
		{
			createNoRecordDisplay();
		}
	}
	else
	{
		console.log("Error trying to load data from Cheng's display: " + AJAX.status);
		//createNoRecordDisplay();
	}
}

function parseTwitterDate(str)
{
	
	var newDate = twitterDateParser(str);
	
	if (!newDate)
		console.error("Warning: unable to properly parse Twitter timestamp: " + str);
	
	return newDate;
}

function parseShortDate(str)
{
	
	var newDate = shortDateParser(str);
	
	if (!newDate)
		console.error("Warning: unable to properly parse Wei timestamp: " + str);
	
	return newDate;
}

function createProfileDisplay()
{
	console.log("Filling in profile display");
	var div = d3.select("#"+css_id.profile + " ." + css_class.inner);
	div.append("img")
		.attr("src",user.profile.profile_image_url)
		.attr("class",css_class.profile_image);
	//console.log(user.profile);
	div.append("table")
		.attr("class",css_class.info_table)
		.append("tbody")
		.selectAll("tr")
		.data(Object.keys(profile_display_fields))
		.enter()
		.append("tr")
		.html(function(d){
			return '<td class="'+css_class.info_td+'">'+d+':<t></td><td>'+user.profile[profile_display_fields[d]]+'</td>';
		});
			
}

function createNoRecordDisplay()
{
	console.log("Filling in no record display");
	d3.select("#"+css_id.profile + " ." + css_class.inner)
		.append("text")
		.text("No profile found with ID " + user_id);
}

function createProfileTimeLineDisplay()
{
	
}

function createTweetHistoryDisplay()
{
	createCumulativeTweetHistory();
	createDayByDayTweetHistory();
	showCumulativeTweetHistory();
}

function createFollowerHistoryDisplay()
{
	createCumulativeFollowerHistory();
	createDayByDayFollowerHistory();
	showCumulativeFollowerHistory();
}

function createFollowedHistoryDisplay()
{
	createCumulativeFollowedHistory();
	createDayByDayFollowedHistory();
	showCumulativeFollowedHistory();
}

function createTweetListDisplay()
{
	createItemListDisplay(css_id.tweet_list, tweet_list_columns, user.tweets);

}

function createCountHistory(attach_div_id, my_svg_id, datalist, baseNum, itemName, plotType)
{

	if (baseNum)
	{
		d3.select("#" + attach_div_id + " ." + css_class.inner)
			.append("div")
			.html(baseNum+" "+itemName+" reported at first observed date: " + dateAndTimeNoCR(datalist[0].values[0].date) + "<br>Cumulative plot shows cumulative activity after this date.");
	}

	var color = d3.scale.category10();
	var x = d3.time.scale().range([0, width]);
	var y = d3.scale.linear().range([height, 0]);
	
	
	var xAxis = d3.svg.axis().scale(x)
		.orient("bottom");
		

	xAxis.ticks(d3.time.hours, 12);
	
	var yAxis = d3.svg.axis().scale(y)
		.orient("left").ticks(5);
	
	var valueline = d3.svg.line()
		.x(function(d) { return x(d.date); })
		.y(function(d) { return y(d.value); });
	
	
	var div = d3.select("body")
		.append("div")	
	    .attr("class", "tooltip")				
	    .style("opacity", 0);
	
	var svg = d3.select("#" + attach_div_id + " ." + css_class.inner)
	    .append("svg")
	    .attr("id",my_svg_id)
	    .attr("class",css_class.count_history)
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	
	
	color.domain(datalist.map(function(d){return d.name;}));
	
	
	//console.log("Data list");
	//console.log(datalist);
	
	x.domain(d3.extent(datalist[0].values, function(d) { return d.date; }));
	
	//console.log(x.domain());
	
	y.domain([
	          d3.min(datalist, function(c) { return d3.min(c.values, function(v) { return v.value; }); }),
	          d3.max(datalist, function(c) { return d3.max(c.values, function(v) { return v.value; }); })
	        ]);
	
	svg.append("g")
	    .attr("class", "x axis")
	    .attr("transform", "translate(0," + height + ")")
	    .call(xAxis);
	
	svg.append("g")
	    .attr("class", "y axis")
	    .call(yAxis)
	    .append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 6)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Count");
	
	svg.append("text")
		.text(plotType + " plot")
		.attr("x", (width / 2))             
        .attr("y", 0 + (margin.top / 2))
        .attr("text-anchor", "middle")
        .attr("class", css_class.title);
	
	var series = svg.selectAll(".series")
	    .data(datalist)
	    .enter().append("g")
	    .attr("class", "series");
	
	series.append("path")
	    .attr("class", "line")
	    .attr("d", function(d) { 
	    	return valueline(d.values); })
	    .attr("data-legend",function(d) { return d.name;})
	    .style("stroke", function(d) { return color(d.name); });
	
	series.selectAll("dot")
		.data(function(d){return d.values;})
		.enter()
		.append("circle")								
	    .attr("r", 4)
	    //.each(function(d){console.log(d);})
	    .attr("cx", function(d) { return x(d.date); })		 
	    .attr("cy", function(d) { return y(d.value); })		
	    .on("mouseover", function(d) {		
	        div.transition()		
	            .duration(200)		
	            .style("opacity", .9);		
	        div.html(d.desc + "<br/><b>("  + d.value+")</b>")	
	            .style("left", (d3.event.pageX) + "px")		
	            .style("top", (d3.event.pageY - 28) + "px");	
	        })					
	    .on("mouseout", function(d) {		
	        div.transition()		
	            .duration(500)		
	            .style("opacity", 0);	
	    });
	
	
	var legend = svg.append("g")
	    .attr("class","legend")
	    .attr("transform","translate(50,30)")
	    .style("font-size","12px")
	    .call(d3.legend);
	
	

}




function createCumulativeTweetHistory()
{
	var datalist = [{name:'All tweets',values:[]},
	                {name:'Original tweets',values:[]},
	                {name:'Retweets',values:[]},
	                {name:'Replies',values:[]},
	                {name:'Reported tweets',values:[]}];
	
	var baseNum = user.count_history[0].statuses_count;
	var tweetIndex = 0;
	var tweet = null;
	var cumVals = [0,0,0,0];
	for (var i = 0; i < user.count_history.length; i++)
	{
		var hist = user.count_history[i];
		var date = parseShortDate(hist.time);
		while (tweetIndex < user.tweets.length && parseTwitterDate(user.tweets[tweetIndex].created_at).getTime() < date.getTime())
		{
			tweet = user.tweets[tweetIndex];
			
			cumVals[0]++;
			if (tweet.retweet_status_id)
				cumVals[2]++;
			else if (tweet.in_reply_to_user_id || tweet.in_reply_to_status_id)
				cumVals[3]++;
			else
				cumVals[1]++;
			
			tweetIndex++;
		}
		
		datalist[0].values.push({date:date,value:cumVals[0], desc: "By " + dateAndTime(date)});	
		datalist[1].values.push({date:date,value:cumVals[1], desc: "By " + dateAndTime(date)});
		datalist[2].values.push({date:date,value:cumVals[2], desc: "By " + dateAndTime(date)});
		datalist[3].values.push({date:date,value:cumVals[3], desc: "By " + dateAndTime(date)});
		datalist[4].values.push({date:date,value:hist.statuses_count-baseNum, desc: "By " + dateAndTime(date)});

		
	}
	createCountHistory(css_id.tweet_history,css_id.cumulative_tweet_history,datalist,baseNum,"tweets", "Cumulative");

}

function createDayByDayTweetHistory()
{
	var datalist = [{name:'All tweets',values:[]},
	                {name:'Original tweets',values:[]},
	                {name:'Retweets',values:[]},
	                {name:'Replies',values:[]},
	                {name:'Reported tweets',values:[]}];
	
	var baseNum = user.count_history[0].statuses_count;
	var tweetIndex = 0;
	var tweet = null;
	var cumVals = [0,0,0,0];
	for (var i = 0; i < user.count_history.length; i++)
	{
		var hist = user.count_history[i];
		var date = parseShortDate(hist.time);
		while (tweetIndex < user.tweets.length && parseTwitterDate(user.tweets[tweetIndex].created_at).getTime() < date.getTime())
		{
			tweet = user.tweets[tweetIndex];
			
			cumVals[0]++;
			if (tweet.retweet_status_id)
				cumVals[2]++;
			else if (tweet.in_reply_to_user_id || tweet.in_reply_to_status_id)
				cumVals[3]++;
			else
				cumVals[1]++;
			
			tweetIndex++;
		}
		
		if (i >0)
		{
			datalist[0].values.push({date:date,value:cumVals[0], desc: dateAndTime(parseShortDate(user.count_history[i-1].time)) + "<br>to<br>" + dateAndTime(date)});	
			datalist[1].values.push({date:date,value:cumVals[1], desc: dateAndTime(parseShortDate(user.count_history[i-1].time)) + "<br>to<br>" + dateAndTime(date)});
			datalist[2].values.push({date:date,value:cumVals[2], desc: dateAndTime(parseShortDate(user.count_history[i-1].time)) + "<br>to<br>" + dateAndTime(date)});
			datalist[3].values.push({date:date,value:cumVals[3], desc: dateAndTime(parseShortDate(user.count_history[i-1].time)) + "<br>to<br>" + dateAndTime(date)});
			datalist[4].values.push({date:date,value:hist.statuses_count-user.count_history[i-1].statuses_count, desc: dateAndTime(parseShortDate(user.count_history[i-1].time)) + "<br>to<br>" + dateAndTime(date)});
			cumVals = [0,0,0,0];
		}
	}
	createCountHistory(css_id.tweet_history,css_id.daybyday_tweet_history,datalist,null,"tweets","Interval");
}

function roundDate(date)
{
	var newDate = new Date(date);
	//newDate.setHours(0);
	newDate.setMinutes(0);
	newDate.setSeconds(0);
	newDate.setMilliseconds(0);
	
	//console.log("Noonified " + date + "<br>to<br>" + newDate);
	return newDate;
}





function createCumulativeFollowerHistory()
{
	console.log("Creating cumulative follower history");
	var datalist = [{name:'Followers',values:[]}];
	var cumVals = [0];
	var baseNum = user.count_history[0].followers_count;

	for (var i = 0; i < user.count_history.length; i++)
	{
		var count_history = user.count_history[i];
		
		cumVals[0] = count_history.followers_count;

		datalist[0].values.push({date:parseShortDate(count_history.time),
			value:cumVals[0]-baseNum, 
			desc:"By " + dateAndTime(parseShortDate(count_history.time))});	
		
	}
	
	createCountHistory(css_id.follower_history,css_id.cumulative_follower_history,datalist,baseNum,"followers","Cumulative");
}

function createDayByDayFollowerHistory()
{
	console.log("Creating day by day follower history");
	var datalist = [{name:'Followers',values:[]}];
	var cumVals = [0];

	for (var i = 1; i < user.count_history.length; i++)
	{
		var count_history = user.count_history[i];
		var prev_history = user.count_history[i-1];
		cumVals[0] = count_history.followers_count-prev_history.followers_count;

		datalist[0].values.push({date:parseShortDate(count_history.time),
			value:cumVals[0],
			desc:dateAndTime(parseShortDate(prev_history.time)) + "<br>to<br>" + dateAndTime(parseShortDate(count_history.time))});	
		
	}


	createCountHistory(css_id.follower_history,css_id.daybyday_follower_history,datalist,null,"followers","Interval");
}

function createCumulativeFollowedHistory()
{
	console.log("Creating cumulative followed history");
	var datalist = [{name:'Followed',values:[]}];
	var cumVals = [0];
	var baseNum = user.count_history[0].friends_count;
	for (var i = 0; i < user.count_history.length; i++)
	{
		var count_history = user.count_history[i];
		
		cumVals[0] = count_history.friends_count;

		datalist[0].values.push({date:parseShortDate(count_history.time),
			value:cumVals[0]-baseNum,
			desc:"By " + dateAndTime(parseShortDate(count_history.time))});	
		
	}
	
	createCountHistory(css_id.followed_history,css_id.cumulative_followed_history,datalist,baseNum,"followed by this user","Cumulative");
}

function createDayByDayFollowedHistory()
{
	console.log("Creating day by day followed history");
	var datalist = [{name:'Followed',values:[]}];
	var cumVals = [0];
	
	for (var i = 1; i < user.count_history.length; i++)
	{
		var count_history = user.count_history[i];
		var prev_history = user.count_history[i-1];

		cumVals[0] = count_history.friends_count-prev_history.friends_count;

		datalist[0].values.push({date:parseShortDate(count_history.time),
			value:cumVals[0],
			desc:dateAndTime(parseShortDate(prev_history.time)) + "<br>to<br>" + dateAndTime(parseShortDate(count_history.time))});	
		
	}

	createCountHistory(css_id.followed_history,css_id.daybyday_followed_history,datalist,false,"followed by this user","Interval");
}



function createItemListDisplay(div_id, column_object, data_list)
{
	console.log("Constructing tweet list");
	var sums = {};
	var div = d3.select("#"+css_id.item_lists + " ."+css_class.inner)
		.append("div")
		.attr("id",div_id)
		.attr("class",css_class.item_list);
	
	div.append("table")
		.append("thead")
		.append("tr")
		.selectAll("th")
		.data(Object.keys(column_object))
		.enter()
		.append("th")
		.each(function(d){sums[d] = 0;})
		.text(function(d){return d;});
	
	div.select("table")
		.append("tbody")
		.selectAll("tr")
		.data(data_list)
		.enter()
		.append("tr");
	
	div.select("tbody")
		.selectAll("tr")
		.selectAll("td")
		.data(function(d, i)
		{
			//console.log(d);
			return Object.keys(column_object).map(function(colname)
				{
					if (typeof column_object[colname](d).value == "string")
					{
						if (i == 0)
							sums[colname] = column_object[colname](d).value;
					}
					else
						sums[colname] += column_object[colname](d).value;
					
					return column_object[colname](d).content;
				});
		})
		//.data(Object.keys(tweet_list_columns))
		.enter()
		.append("td")
		.text(function(d){return d;});
	
	$("#"+div_id+" table").tablesorter(); 
	
	div.select("table thead")
		.append("tr")
		.selectAll("td")
		.data(Object.keys(column_object))
		.enter()
		.append("td")
		.text(function(d){return sums[d];});

	//console.log(sums);
}

function createFollowerListDisplay()
{
	createItemListDisplay(css_id.follower_list,follower_list_columns,user.followers);
}

function createFollowedListDisplay()
{
	createItemListDisplay(css_id.followed_list,followed_list_columns,user.followed);

}

function geoLookup(lat,long)
{
	if (lat && long)
		return "Not implemented";
	else
		return "None";
}


function showList(button_id, list_id)
{
	console.log("Showing edge list");
	d3.selectAll("."+css_class.item_list)
		.attr("class",css_class.item_list + " "+css_class.hidden);
	
	d3.select("#"+list_id)
		.attr("class",css_class.item_list);
	
	d3.selectAll("#"+css_id.list_choice+" td")
		.attr("class",css_class.unselected);
	
	d3.select("#"+button_id)
		.attr("class",css_class.selected);
}



function showCountHistory(container_div_id,svg_id)
{
	d3.selectAll("#" + container_div_id+" ."+css_class.count_history)
		.attr("class",css_class.count_history+" "+css_class.hidden);

	d3.select("#" + svg_id)
		.attr("class",css_class.count_history);
}

function showCumulativeTweetHistory()
{
	console.log("Showing cumulative tweet history");
	showCountHistory(css_id.tweet_history, css_id.cumulative_tweet_history);

}

function showDayByDayTweetHistory()
{
	console.log("Showing day by day tweet history");

	showCountHistory(css_id.tweet_history, css_id.daybyday_tweet_history);
}

function showCumulativeFollowerHistory()
{
	console.log("Showing cumulative follower history");
	showCountHistory(css_id.follower_history, css_id.cumulative_follower_history);
}

function showDayByDayFollowerHistory()
{
	console.log("Showing day by day follower history");

	showCountHistory(css_id.follower_history, css_id.daybyday_follower_history);
}

function showCumulativeFollowedHistory()
{
	console.log("Showing cumulative followed history");
	showCountHistory(css_id.followed_history, css_id.cumulative_followed_history);
}

function showDayByDayFollowedHistory()
{
	console.log("Showing day by day followed history");

	showCountHistory(css_id.followed_history, css_id.daybyday_followed_history);
}


function createXMLHttpRequest()
{
	// See http://en.wikipedia.org/wiki/XMLHttpRequest
	// Provide the XMLHttpRequest class for IE 5.x-6.x:
	if (typeof XMLHttpRequest == "undefined")
		XMLHttpRequest = function()
		{
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP.6.0");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP.3.0");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
			}
			throw new Error("This browser does not support XMLHttpRequest.");
		};
	return new XMLHttpRequest();
}