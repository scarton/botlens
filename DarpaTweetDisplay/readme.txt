This project is for the DARPA bot detection challenge. 

It is an interface to our database/the DARPA-provided API. It is meant to allow
a person to input a Twitter user ID, and see a display of information about that
person. This is to allow for human judgments of bot-ness to develop intuitions
to guide our design of automatic methods. 